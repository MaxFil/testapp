//
//  Contacts.swift
//  TestApp
//
//  Created by Максим Филимонов on 04.11.2017.
//  Copyright © 2017 Максим Филимонов. All rights reserved.
//

import UIKit

class Contacts: NSObject {
    
    var delegateVC: UIViewController?
    //Массив в фейковыми контактами
    private var fakeContacts = [Contact]()
    
    //Отсортированые контакты
    var sortedContacts = [String:[Contact]]()
    
    //Ключи из словаря(первые буквы имени контакта)
    var keys = Array<String>()
    
    
    init(delegate: UIViewController) {
        self.delegateVC = delegate
        super.init()
        //Добавляем контакты
        addFakeContacts()
        //Сортируем
        sortingContacts()
    }

    private func addFakeContacts() {
        let firstNameArray = ["Jasmine","Anne", "Boris", "Liam", "Jane", "Nicholas","Olivia","Colin","Eric", "Максим"]
        let lastNameArray = ["Roberts", "Skinner", "Gibson", "Ball", "Langdon", "Carr", "Quinn", "Blake", "Cameron", "Филимонов"]
        let phoneArray = ["+1-202-555-0174", "+1-202-555-0100", "+1-202-555-0178", "+1-202-555-0127", "+1-202-555-0106", "+1-202-555-0144",
                          "+1-202-555-0108", "+1-202-555-0194", "+1-202-555-0121", "+7-977-395-60-27"]
        
        for i in 0..<firstNameArray.count {
            let contact = Contact()
            contact.firstName = firstNameArray[i]
            contact.lastName = lastNameArray[i]
            contact.phone = phoneArray[i]
            
            self.fakeContacts.append(contact)
        }
    }
    
    private func sortingContacts() {
        
        for i in 0..<self.fakeContacts.count {
            let contact = self.fakeContacts[i]
        
            if let fName = contact.firstName {
                let key = String(fName.uppercased().characters.first!)
               
                if sortedContacts[key] != nil {
                    sortedContacts[key]?.append(contact)
                }
                else {
                    sortedContacts[key] = [contact]
                    keys.append(key)
                }
            }
            
            else {
                if let lName = contact.lastName {
                    let key = String(lName.uppercased().characters.first!)
                    if sortedContacts[key] != nil {
                        sortedContacts[key]?.append(contact)
                        
                    }
                    else {
                        sortedContacts[key] = [contact]
                        keys.append(key)
                    }
                }
            }
        }
        keys.sort()
        
        guard let delegate = delegateVC as? ContactsTableViewController else {
            return
        }
        delegate.tableView.reloadData()
    }
}
