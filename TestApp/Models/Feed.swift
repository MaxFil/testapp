//
//  Feed.swift
//  TestApp
//
//  Created by Максим Филимонов on 03.11.2017.
//  Copyright © 2017 Максим Филимонов. All rights reserved.
//

import UIKit

class Feed: NSObject {
    var posts: [Post]?
    var users: [User]?
    var comments: [Comment]?
    var photos: [Photo]?
    var toDos: [ToDo]?
    
    //Массив для хранения типов моделей, которые успешно загрузились
    var loadedModelTypes: [ModelType]?
    
    override init() {
        self.loadedModelTypes = [ModelType]()
    }
}
