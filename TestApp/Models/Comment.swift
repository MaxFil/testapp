//
//  Comment.swift
//  TestApp
//
//  Created by Максим Филимонов on 31.10.2017.
//  Copyright © 2017 Максим Филимонов. All rights reserved.
//

import UIKit

class Comment: NSObject {
    
    //Commented post id
    var postId: Int?
    
    //Comment id
    var id: Int?
    //Comment name
    var name: String?
    //User email
    var email: String?
    //Comment text
    var body: String?
    
    //Commented post
    var commentPost: Post?
    
}
