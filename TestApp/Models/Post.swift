//
//  Post.swift
//  TestApp
//
//  Created by Максим Филимонов on 30.10.2017.
//  Copyright © 2017 Максим Филимонов. All rights reserved.
//

import UIKit

class Post: NSObject {
    //Post id
    var id: Int?
    //Post titile
    var title: String?
    //Post text
    var body: String?
    //User id
    var userId: Int?
    
    //User
    var user: User?
    
}
