//
//  ToDo.swift
//  TestApp
//
//  Created by Максим Филимонов on 01.11.2017.
//  Copyright © 2017 Максим Филимонов. All rights reserved.
//

import UIKit

class ToDo: NSObject {
    
    //User id
    var userId: Int?
    //Task id
    var id: Int?
    //Task title
    var title: String?
    //Task status
    var completed: Bool?
    
    //User
    var user: User?
}
