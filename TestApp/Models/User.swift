//
//  User.swift
//  TestApp
//
//  Created by Максим Филимонов on 30.10.2017.
//  Copyright © 2017 Максим Филимонов. All rights reserved.
//

import UIKit

class User: NSObject {
    
    //User id
    var id: Int?
    //User full name
    var name: String?
    //User nickname
    var username: String?
    
    //User email
    var email: String?
    //User address
    var address: [String: AnyObject]?
    //User phone
    var phone: String?
    //User website
    var website: String?
    //User company
    var company: [String: AnyObject]?
    
}
