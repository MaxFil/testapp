//
//  Contact.swift
//  TestApp
//
//  Created by Максим Филимонов on 02.11.2017.
//  Copyright © 2017 Максим Филимонов. All rights reserved.
//

import UIKit

class Contact: NSObject {
    
    var firstName: String?
    var lastName: String?
    
    var phone: String?
    
}
