//
//  Photo.swift
//  TestApp
//
//  Created by Максим Филимонов on 01.11.2017.
//  Copyright © 2017 Максим Филимонов. All rights reserved.
//

import UIKit

class Photo: NSObject {
    //Photo album id
    var albumId: Int?
    //Photi id
    var id: Int?
    //Photo title
    var title: String?
    //Photo url
    var url: String?
    //Photo thumbnail url
    var thumbnailUrl: String?
    //Loaded image
    var loadedPhoto: UIImage?
}
