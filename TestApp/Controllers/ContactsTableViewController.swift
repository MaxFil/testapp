//
//  ContactsTableViewController.swift
//  TestApp
//
//  Created by Максим Филимонов on 30.10.2017.
//  Copyright © 2017 Максим Филимонов. All rights reserved.
//

import UIKit

class ContactsTableViewController: UITableViewController {
    
    //Массив в фейковыми контактами
    private var fakeContacts = [Contact]()
    
    //Отсортированые контакты
    private var sortedContacts = [String:[Contact]]()
    
    //Ключи из словаря(первые буквы имени контакта)
    private var keys = Array<String>()
    
    //Модель хранения данных
    private var contacts: Contacts?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.contacts = Contacts.init(delegate: self)
        
        configuredTableView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.contacts?.sortedContacts.count ?? 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let contactsModel = contacts else {
            return 0
        }
        //Определяем ключ для секции, например "B"
        let key = contactsModel.keys[section]
        //Узнаем в словаре сколько контактов в массиве под ключем "B"
        let number = contactsModel.sortedContacts[key]?.count
        return number ?? 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ContactTableViewCell.self), for: indexPath) as! ContactTableViewCell
        
        guard let contactsModel = self.contacts else {
            return cell
        }
        //Определяем ключ для секции
        let key = contactsModel.keys[indexPath.section]
        
        //Берем нужный объект
        if let array = contactsModel.sortedContacts[key] {
            cell.contact = array[indexPath.row]
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.contacts?.keys[section]
    }
    

    
    private func configuredTableView() {
        let contactCell = UINib(nibName: String(describing: ContactTableViewCell.self), bundle: nil)
        self.tableView.register(contactCell, forCellReuseIdentifier:  String(describing: ContactTableViewCell.self))

        self.tableView.delegate = self
        self.tableView.dataSource = self
        
    }
    
    

}
