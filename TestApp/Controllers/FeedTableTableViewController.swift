//
//  FeedTableTableViewController.swift
//  TestApp
//
//  Created by Максим Филимонов on 30.10.2017.
//  Copyright © 2017 Максим Филимонов. All rights reserved.
//

import UIKit

enum ModelType {
    case Post, User, Comment, Photo, ToDo, All
}

class FeedTableTableViewController: UITableViewController {
    //Ссылки на ресурсы
    private let postsUrl = "https://jsonplaceholder.typicode.com/posts"
    private let usersUrl = "https://jsonplaceholder.typicode.com/users"
    private let commentsUrl = "https://jsonplaceholder.typicode.com/comments"
    private let photosUrl = "https://jsonplaceholder.typicode.com/photos"
    private let toDosUrl = "https://jsonplaceholder.typicode.com/todos"
    
    //Модель для хранения данных
    private var feed = Feed.init()
    
    //Вьюхи для отображения загрузки или ошибки
    private var loadingView: LoadingView?
    private var errorMessageView: ErrorMessageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadingView = LoadingView.instanceFromNib()
        self.errorMessageView = ErrorMessageView.instanceFromNib()
        
        configuredTableView()
        
        self.loadingView(isShow: true)
        fetchData(of: .All, with: 55)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func fetchData(of type: ModelType, with id: Int) {
        let dispatchGroup = DispatchGroup()
        
        switch type {
        case .Post:
            dispatchGroup.enter()
            getPost(with: id, closure: {
                dispatchGroup.leave()
            })
            
            dispatchGroup.notify(queue: .main) {
                self.tableView.reloadData()
            }
            
        case .User:
            print("Cant fetch user")
            
        case .Comment:
            dispatchGroup.enter()
            getComment(with: id, closure: {
                dispatchGroup.leave()
            })
            
            dispatchGroup.notify(queue: .main) {
                self.tableView.reloadData()
            }
            
        case .Photo:
            print("Cant fetch photos")
        case .ToDo:
            print("Cant fetch ToDo")
            
        case .All:
            //Запрос конкретного поста по id
            dispatchGroup.enter()
            getPost(with: id, closure: {
                dispatchGroup.leave()
            })
            
            //Запрос конкретного комментария по id
            dispatchGroup.enter()
            getComment(with: id, closure: {
                dispatchGroup.leave()
            })
            
            //Запрос юзера с id 1
            dispatchGroup.enter()
            getUser(with: 1, closure: {
                dispatchGroup.leave()
            })
            
            //Запрос юзера с id 2
            dispatchGroup.enter()
            getUser(with: 2, closure: {
                dispatchGroup.leave()
            })
            
            //Запрос юзера с id 3
            dispatchGroup.enter()
            getUser(with: 3, closure: {
                dispatchGroup.leave()
            })
            
            //Запрос юзера с id 4
            dispatchGroup.enter()
            getUser(with: 4, closure: {
                dispatchGroup.leave()
            })
            
            //Запрос юзера с id 5
            dispatchGroup.enter()
            getUser(with: 5, closure: {
                dispatchGroup.leave()
            })
            
            //Запрос 3-го фото
            dispatchGroup.enter()
            getPhoto(with: 3, closure: {
                dispatchGroup.leave()
            })
            
            //Запрос задачи
            dispatchGroup.enter()
            getToDo(with: 7, closure: {
                dispatchGroup.leave()
            })
            
            dispatchGroup.notify(queue: .main) {
                //Если не удалось загрузить данные отображаем экран с ошибкой "App cannot load data"
                if self.feed.loadedModelTypes?.count == 0 {
                    self.loadingView(isShow: false)
                    self.errorMessageView(isShow: true)
                }
                else {
                    self.loadingView(isShow: false)
                    self.tableView.reloadData()
                }
            }
            
        }
    }
    
    // MARK: - Методы для запросов
    
    //Получение объекта выполняется в двух методах. Get-метод запрашивает объект через fetch-метод по id и добавляет в массив с данными, fetch-метод возвращает объект, либо nil
    private func getPost(with id: Int, closure: @escaping () -> Void) {
        print("GET_POST")
        fetchPost(with: id) { (fetchedPost) in
            
            guard let post = fetchedPost else {
                //Объекта нет, выходим из метода
                closure()
                return
            }
            //Если массив инициализирован ранее, значит удаляем старые данные и добавляем новые
            guard self.feed.posts != nil else {
                self.feed.posts = [Post]()
                self.feed.posts?.append(post)
                //Чекаем что хотя бы один объект был загружен и добавлен
                self.feed.loadedModelTypes?.append(.Post)
                closure()
                return
            }
            self.feed.posts?.removeAll()
            self.feed.posts?.append(post)
            closure()
            
        }
        
    }
    
    private func getComment(with id: Int, closure: @escaping () -> Void) {
        print("GET_COMMENT")
        fetchComment(with: id) { (fetchedComment) in
           
            guard let comment = fetchedComment else {
                closure()
                return
            }
            //Если массив инициализирован ранее, значит удаляем старые данные и добавляем новые
            guard self.feed.comments != nil else {
                self.feed.comments = [Comment]()
                self.feed.comments?.append(comment)
                //Чекаем что хотя бы один объект был загружен и добавлен
                self.feed.loadedModelTypes?.append(.Comment)
                closure()
                return
            }
            self.feed.comments?.removeAll()
            self.feed.comments?.append(comment)
            closure()
        }
    }
    
    private func getUser(with id: Int, closure: @escaping () -> Void) {
        print("GET_USER")
        self.fetchUser(with: id) { (fetchedUser) in
            
            guard let user = fetchedUser else {
                closure()
                return
            }
            //Если массив инициализирован ранее, значит удаляем старые данные и добавляем новые
            guard self.feed.users != nil else{
                self.feed.users = [User]()
                self.feed.users?.append(user)
                //Чекаем что хотя бы один объект был загружен и добавлен
                self.feed.loadedModelTypes?.append(.User)
                closure()
                return
            }
            self.feed.users?.append(user)
            closure()
        }
    }
    
    private func getPhoto(with id: Int, closure: @escaping () -> Void) {
        print("GET_PHOTO")
        self.fetchPhoto(with: id) { (fetchedPhoto) in
            guard let photo = fetchedPhoto else {
                closure()
                return
            }
            //Если массив инициализирован ранее, значит удаляем старые данные и добавляем новые
            guard self.feed.photos != nil else{
                self.feed.photos = [Photo]()
                self.feed.photos?.append(photo)
                //Чекаем что хотя бы один объект был загружен и добавлен
                self.feed.loadedModelTypes?.append(.Photo)
                closure()
                return
            }
            self.feed.photos?.removeAll()
            self.feed.photos?.append(photo)
            closure()
        }
    }
    
    private func getToDo(with id: Int, closure: @escaping () -> Void) {
        print("GET_TODO")
        fetchToDo(with: id) { (fetchedToDo) in
            
            guard let toDo = fetchedToDo else {
                closure()
                return
            }
            //Если массив инициализирован ранее, значит удаляем старые данные и добавляем новые
            guard self.feed.toDos != nil else{
                self.feed.toDos = [ToDo]()
                self.feed.toDos?.append(toDo)
                //Чекаем что хотя бы один объект был загружен и добавлен
                self.feed.loadedModelTypes?.append(.ToDo)
                closure()
                return
            }
            self.feed.toDos?.removeAll()
            self.feed.toDos?.append(toDo)
            closure()
        }
        
    }
    
    //MARK: - Методы получения объектов
    private func fetchPost(with id: Int, completionHandler: @escaping (Post?) -> Void) {
        //Ссылка на ресурс
        let url = URL(string:"\(postsUrl)/\(id)")
        
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            guard error == nil else {
                //Если ошибка загрузки, возвращаем nil и выходим из метода
                completionHandler(nil)
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                
                let post = Post()
                post.userId = json["userId"] as? Int ?? nil
                post.id = json["id"] as? Int ?? nil
                post.title = json["title"] as? String ?? nil
                post.body = json["body"] as? String ?? nil
                
                guard let userId = post.userId else {
                    completionHandler(post)
                    return
                }
                
                //Запрашиваем юзера по id
                self.fetchUser(with: userId, completionHandler: { (fetchedUser) -> Void in
                    guard let user = fetchedUser else {
                        //Юзер не загружен, выходим из метода
                        completionHandler(post)
                        return
                    }
                    post.user = user
                    //Юзер получен и добавлен в объект Post, выходим из метода
                    completionHandler(post)
                    return
                    
                })
                
            } catch let error as NSError{
                print(error)
                completionHandler(nil)
                return
            }
            
        }).resume()
        
    }
    
    
    private func fetchComment(with id: Int, completionHandler: @escaping (Comment?) -> Void) {
        let url = URL(string:"\(commentsUrl)/\(id)")
        
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            guard error == nil else {
                completionHandler(nil)
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                
                let comment = Comment()
                comment.id = json["id"] as? Int ?? nil
                comment.name = json["name"] as? String ?? nil
                comment.body = json["body"] as? String ?? nil
                comment.email = json["email"] as? String ?? nil
                comment.postId = json["postId"] as? Int ?? nil
                
                guard let postId = comment.postId else {
                    completionHandler(comment)
                    return
                }
                
                //Запрашиваем прокомментированный пост по id
                self.fetchPost(with: postId, completionHandler: { (fetchedPost) -> Void in
                    guard let post = fetchedPost else{
                        //Пост не загружен, выходим из метода
                        completionHandler(comment)
                        return
                    }
                    comment.commentPost = post
                    //Пост загружен добавляем в объект Comment, выходим из метода
                    completionHandler(comment)
                    return
                })
                
            } catch let error as NSError{
                print(error)
                completionHandler(nil)
                return
            }
            
        }).resume()
    }
    
    
    private func fetchUser(with id: Int, completionHandler: @escaping (User?) -> Void) {
        let url = URL(string:"\(usersUrl)/\(id)")
        
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            print("USER_ID:\(id)")
            guard error == nil else {
                completionHandler(nil)
                return
            }
            
            do{
                let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                
                let user = User()
                user.id = json["id"] as? Int ?? nil
                user.name = json["name"] as? String ?? nil
                user.username = json["username"] as? String ?? nil
                user.email = json["email"] as? String ?? nil
                user.phone = json["phone"] as? String ?? nil
                user.address = json["address"] as? [String: AnyObject] ?? nil
                user.company = json["company"] as? [String: AnyObject] ?? nil
                user.website = json["website"] as? String ?? nil
                
                completionHandler(user)
                return
            } catch let error as NSError {
                print(error)
                completionHandler(nil)
                return
            }
        }).resume()
    }
    
    
    private func fetchPhoto(with id: Int, completionHandler: @escaping (Photo?) -> Void) {
        let url = URL(string: "\(photosUrl)/\(id)")
        
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            
            guard error == nil else {
                completionHandler(nil)
                return
            }
            
            do{
                let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                
                let photo = Photo()
                photo.albumId = json["albumId"] as? Int ?? nil
                photo.id = json["id"] as? Int ?? nil
                photo.title = json["title"] as? String ?? nil
                photo.url = json["url"] as? String ?? nil
                photo.thumbnailUrl = json["thumbnailUrl"] as? String ?? nil
                
                guard let photoUrl = photo.url else {
                    completionHandler(photo)
                    return
                }
                
                //Загружаем и добавляем изображение в объект Photo
                self.loadingPhoto(from: photoUrl, completionHandler: { (image) in
                    guard image != nil else {
                        completionHandler(photo)
                        return
                    }
                    photo.loadedPhoto = image
                    completionHandler(photo)
                    return
                })
                
            } catch let error as NSError {
                print(error)
                completionHandler(nil)
                return
            }
        }).resume()
        
        
    }
    
    //метод для загрузки изображения по ссылке
    private func loadingPhoto(from url: String, completionHandler: @escaping (UIImage?) -> Void) {
        let url = URL(string: url)
        
        guard url != nil else {
            completionHandler(nil)
            return
        }
        
        do{
            let data = try Data(contentsOf: url!)
            if let image = UIImage(data: data) {
                completionHandler(image)
                return
            }
            else {
                completionHandler(nil)
                return
            }
        } catch let error as NSError {
            print(error)
            completionHandler(nil)
            return
        }
        
    }
    
    
    private func fetchToDo(with id: Int, completionHandler: @escaping (ToDo?) -> Void) {
        let url = URL(string:"\(toDosUrl)/\(id)")
        
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            guard error == nil else {
                completionHandler(nil)
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                
                let toDo = ToDo()
                toDo.userId = json["userId"] as? Int ?? nil
                toDo.id = json["id"] as? Int ?? nil
                toDo.title = json["title"] as? String ?? nil
                toDo.completed = json["completed"] as? Bool ?? nil
                
                //Получаем юзера по id
                guard let userId = toDo.userId else {
                    completionHandler(toDo)
                    return
                }
                
                self.fetchUser(with: userId, completionHandler: { (fetchedUser) -> Void in
                    guard fetchedUser != nil else {
                        completionHandler(toDo)
                        return
                    }
                    toDo.user = fetchedUser
                    completionHandler(toDo)
                    return
                })
                
            } catch let error as NSError{
                print(error)
                completionHandler(nil)
                return
            }
            
        }).resume()
        
    }
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.feed.posts?.count ?? 0
        case 1:
            return self.feed.comments?.count ?? 0
        case 2:
            return self.feed.users?.count ?? 0
        case 3:
            return self.feed.photos?.count ?? 0
        case 4:
            return self.feed.toDos?.count ?? 0
        default:
            return 0
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PostTableViewCell.self), for: indexPath) as! PostTableViewCell
            
            cell.post = self.feed.posts?[indexPath.row]
            cell.dataSource = self
            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CommentTableViewCell.self), for: indexPath) as! CommentTableViewCell
            cell.comment = self.feed.comments?[indexPath.row]
            cell.dataSource = self
            
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: UserTableViewCell.self), for: indexPath) as! UserTableViewCell
            cell.user = self.feed.users?[indexPath.row]
            
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PhotoTableViewCell.self), for: indexPath) as! PhotoTableViewCell
            cell.photo = self.feed.photos?[indexPath.row]
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ToDoTableViewCell.self), for: indexPath) as! ToDoTableViewCell
            
            cell.toDo = self.feed.toDos?[indexPath.row]
            return cell
        default:
            return cell
            
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        switch section {
        case 0:
            guard self.feed.posts != nil else {
                return ""
            }
            return "Post"
        case 1:
            guard self.feed.comments != nil else {
                return ""
            }
            return "Comment"
        case 2:
            guard self.feed.users != nil else {
                return ""
            }
            return "Users"
        case 3:
            guard self.feed.photos != nil else {
                return ""
            }
            return "Photo"
        case 4:
            guard self.feed.toDos != nil else {
                return ""
            }
            return "Task"
        default:
            return ""
        }
    }
    
    
    
    
    private func configuredTableView() {
        let postCell = UINib(nibName: String(describing: PostTableViewCell.self), bundle: nil)
        self.tableView.register(postCell, forCellReuseIdentifier:  String(describing: PostTableViewCell.self))
        
        let commentCell = UINib(nibName: String(describing: CommentTableViewCell.self), bundle: nil)
        self.tableView.register(commentCell, forCellReuseIdentifier:  String(describing: CommentTableViewCell.self))
        
        let userCell = UINib(nibName: String(describing: UserTableViewCell.self), bundle: nil)
        self.tableView.register(userCell, forCellReuseIdentifier:  String(describing: UserTableViewCell.self))
        
        let photoCell = UINib(nibName: String(describing: PhotoTableViewCell.self), bundle: nil)
        self.tableView.register(photoCell, forCellReuseIdentifier:  String(describing: PhotoTableViewCell.self))
        
        let toDoCell = UINib(nibName: String(describing: ToDoTableViewCell.self), bundle: nil)
        self.tableView.register(toDoCell, forCellReuseIdentifier:  String(describing: ToDoTableViewCell.self))
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 240
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(self.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
        
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func loadingView(isShow: Bool) {
        if let loadingView = self.loadingView {
            if isShow {
                loadingView.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height)
                loadingView.backgroundColor = UIColor.white
                loadingView.activityIndicator.startAnimating()
                self.view.layoutIfNeeded()
                self.view.addSubview(loadingView)
                
            }
            else {
                loadingView.removeFromSuperview()
                self.view.layoutIfNeeded()
            }
        }
    }
    
    
    func errorMessageView(isShow: Bool) {
        if let errorMessageView = self.errorMessageView {
            if isShow {
                errorMessageView.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height)
                errorMessageView.backgroundColor = UIColor.white
                errorMessageView.refreshingVC = self
                self.view.addSubview(errorMessageView)
                self.view.layoutIfNeeded()
            }
            else {
                errorMessageView.removeFromSuperview()
                self.view.layoutIfNeeded()
            }
        }
    }
    
}
