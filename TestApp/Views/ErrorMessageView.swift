//
//  ErrorMessageView.swift
//  TestApp
//
//  Created by Максим Филимонов on 03.11.2017.
//  Copyright © 2017 Максим Филимонов. All rights reserved.
//

import UIKit

class ErrorMessageView: UIView {

    @IBOutlet weak var errorMessageLabel: UILabel!
    @IBOutlet weak var retryFetchDataButton: UIButton!
    
    var refreshingVC: UIViewController?
    
    @IBAction func retryFecthData(_ sender: UIButton) {
        if let refreshingVC = self.refreshingVC as? FeedTableTableViewController {
            refreshingVC.errorMessageView(isShow: false)
            refreshingVC.loadingView(isShow: true)
            refreshingVC.fetchData(of: .All, with: 55)
        }
    }
    
    class func instanceFromNib() -> ErrorMessageView {
        
        return UINib(nibName: String(describing: ErrorMessageView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ErrorMessageView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.retryFetchDataButton.layer.masksToBounds = true
        self.retryFetchDataButton.layer.cornerRadius = 4
        self.retryFetchDataButton.layer.borderWidth = 1
        self.retryFetchDataButton.layer.borderColor = UIColor(red: 158/255, green: 158/255, blue: 158/255, alpha: 1.0).cgColor
    }
}
