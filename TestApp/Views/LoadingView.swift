//
//  LoadingView.swift
//  TestApp
//
//  Created by Максим Филимонов on 31.10.2017.
//  Copyright © 2017 Максим Филимонов. All rights reserved.
//

import UIKit

class LoadingView: UIView {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
   
    class func instanceFromNib() -> LoadingView {
        return UINib(nibName: String(describing: LoadingView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! LoadingView
    }
}
