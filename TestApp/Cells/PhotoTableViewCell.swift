//
//  PhotoTableViewCell.swift
//  TestApp
//
//  Created by Максим Филимонов on 01.11.2017.
//  Copyright © 2017 Максим Филимонов. All rights reserved.
//

import UIKit

class PhotoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titlePhotoLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    
    var photo: Photo? {
        didSet{
            if let photoObject = photo {
                self.titlePhotoLabel.text = photoObject.title ?? "No title"
                self.photoImageView.image = photoObject.loadedPhoto
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func noImage() {
        let label = UILabel()
        let description = "Image not loaded"
        label.text = description
        label.font = UIFont.systemFont(ofSize: 14)
        label.center = self.photoImageView.center
        
        self.photoImageView.addSubview(label)

    }
    
}
