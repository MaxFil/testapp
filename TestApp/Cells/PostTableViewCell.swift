//
//  PostTableViewCell.swift
//  TestApp
//
//  Created by Максим Филимонов on 30.10.2017.
//  Copyright © 2017 Максим Филимонов. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {
    //Аватарка юзера
    @IBOutlet weak var userProfileImage: UIImageView!
    //Имя юзера
    @IBOutlet weak var userNameLabel: UILabel!
    //Описание действия юзера
    @IBOutlet weak var userActionLabel: UILabel!
    
    //Заголовок поста
    @IBOutlet weak var titlePostLabel: UILabel!
    //Текст поста
    @IBOutlet weak var messagePostLabel: UILabel!
    
    //Текстовое поле для ввода id
    @IBOutlet weak var postIdtextField: UITextField!
    
    //Кнопка получения поста
    @IBOutlet weak var getButton: UIButton!
    
    var dataSource: UIViewController?
    
    var loadingView: LoadingView?
    
    var post: Post? {
        didSet{
            loadingView(isShow: false)
            if let postObject = post {
                self.titlePostLabel.text = postObject.title ?? "No title"
                self.messagePostLabel.text = postObject.body ?? "No message text"
                self.userNameLabel.text = postObject.user?.name ?? "Anonymous"
                self.userActionLabel.textColor = UIColor.gray
                
                self.setupUserImage(with: nil, or: userNameLabel.text?.capitalized)
            }
        }
    }
    
    @IBAction func getPostButton(_ sender: UIButton) {
        if let dataSource = dataSource as? FeedTableTableViewController {
            //Проверка на пустое поле
            guard postIdtextField.text?.isEmpty == false else {
                dataSource.dismissKeyboard()
                return
            }
            
            //Проверка введеного id
            if let requestPostId = Int(postIdtextField.text!) {
                if requestPostId <= 100 && requestPostId >= 1 {
                    
                    //Убираем клавиатуру
                    dataSource.dismissKeyboard()
                    //Отображаем прямо в ячейке UIView со спинером(ActivityIndicator)
                    loadingView(isShow: true)
                    
                    //Просим dataSource запросить данные
                    dataSource.fetchData(of: .Post, with: requestPostId)
                    
                    //Очищаем поле ввода
                    self.postIdtextField.text = ""
                }
                else {
                    //Если id не корректный, трясем поле
                    shake(textField: postIdtextField)
                }
            }
            
            
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.loadingView = LoadingView.instanceFromNib()
        
        self.userProfileImage.layer.masksToBounds = true
        self.userProfileImage.layer.cornerRadius = self.userProfileImage.frame.height/2
        
        self.getButton.layer.masksToBounds = true
        self.getButton.layer.cornerRadius = 2
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    private func setupUserImage(with image: UIImage?, or name: String?) {
        if let profileImage = image {
            self.userProfileImage.contentMode = .scaleAspectFit
            self.userProfileImage.image = profileImage
        }
        
        //Если нет фото юзера, рисуем первую букву имени
        if let username = name {
            let labelName = UILabel()
            labelName.frame.size = CGSize(width: 40.0, height: 40.0)
            labelName.textColor = UIColor.white
            labelName.text = String(username.characters.first!)
            labelName.textAlignment = NSTextAlignment.center
            labelName.backgroundColor = UIColor(red: 255/255, green: 87/255, blue: 34/255, alpha: 1.0)
            labelName.layer.cornerRadius = 20.0
            
            UIGraphicsBeginImageContextWithOptions(labelName.frame.size, true, UIScreen.main.scale)
            labelName.layer.render(in: UIGraphicsGetCurrentContext()!)
            userProfileImage.image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
        }
    }
    
    private func loadingView(isShow: Bool) {
        if isShow{
            if let loadingView = self.loadingView {
                loadingView.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.width, height: self.frame.height)
                loadingView.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.95)
                self.contentView.addSubview(loadingView)
                self.layoutIfNeeded()
                loadingView.activityIndicator.startAnimating()
            }
        }
        else {
            if let loadingView = self.loadingView {
                loadingView.removeFromSuperview()
            }
        }
    }
    
    private func shake(textField: UITextField) {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.08
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: textField.center.x - 2, y: textField.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: textField.center.x + 2, y: textField.center.y))
        textField.layer.add(animation, forKey: "position")
    }
    
}
