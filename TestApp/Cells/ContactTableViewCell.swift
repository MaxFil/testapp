//
//  ContactTableViewCell.swift
//  TestApp
//
//  Created by Максим Филимонов on 02.11.2017.
//  Copyright © 2017 Максим Филимонов. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userPhoneLabel: UILabel!


    var contact: Contact? {
        didSet{
            if let contactObject = contact,
               let fName = contactObject.firstName,
               let lName = contactObject.lastName {
                
                self.userNameLabel.text = String("\(fName) \(lName)")
                self.userPhoneLabel.text = contactObject.phone
                self.setupUserImage(with: fName)
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.userProfileImage.layer.masksToBounds = true
        self.userProfileImage.layer.cornerRadius = self.userProfileImage.frame.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setupUserImage(with name: String?) {
        
        if let userName = name {
           
            let labelName = UILabel()
            labelName.frame.size = CGSize(width: 40.0, height: 40.0)
            labelName.textColor = UIColor.white
            labelName.text = String(userName.characters.first!)
            labelName.textAlignment = NSTextAlignment.center
            labelName.backgroundColor = UIColor(red: 3/255, green: 168/255, blue: 244/255, alpha: 1.0)
            labelName.layer.cornerRadius = 20.0
            
            UIGraphicsBeginImageContextWithOptions(labelName.frame.size, true, UIScreen.main.scale)
            labelName.layer.render(in: UIGraphicsGetCurrentContext()!)
            userProfileImage.image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
        }
    }
    
    @IBAction func makeCallAction(_sender: UIButton) {
        guard let numberPhone = self.userPhoneLabel.text else { return }
        let editedNumber = numberPhone.replacingOccurrences(of: "-", with: "")
        
        guard let url = URL(string: "TEL://\(editedNumber)") else { return }
        
        if #available(iOS 10.0, *) {
            print("Calling")
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
            UIApplication.shared.openURL(url)
        }
    }
    
}
