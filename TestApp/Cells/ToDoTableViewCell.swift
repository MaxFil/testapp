//
//  ToDoTableViewCell.swift
//  TestApp
//
//  Created by Максим Филимонов on 01.11.2017.
//  Copyright © 2017 Максим Филимонов. All rights reserved.
//

import UIKit

class ToDoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var actionNameLabel: UILabel!
    
    @IBOutlet weak var titleTaskLabel: UILabel!
    
    var toDo: ToDo? {
        didSet{
            if let toDoObject = toDo {
                self.userNameLabel.text = toDoObject.user?.name?.capitalized ?? "Anonymous"
                self.actionNameLabel.textColor = UIColor.gray
                
                self.titleTaskLabel.text = toDoObject.title ?? "No text task"
                self.setupUserImage(with: nil, or: userNameLabel.text?.capitalized)
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.userProfileImage.layer.masksToBounds = true
        self.userProfileImage.layer.cornerRadius = self.userProfileImage.frame.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setupUserImage(with image: UIImage?, or name: String?) {
        if let profileImage = image {
            self.userProfileImage.contentMode = .scaleAspectFit
            self.userProfileImage.image = profileImage
        }
        
        if let username = name {
            let labelName = UILabel()
            labelName.frame.size = CGSize(width: 40.0, height: 40.0)
            labelName.textColor = UIColor.white
            labelName.text = String(username.characters.first!)
            labelName.textAlignment = NSTextAlignment.center
            labelName.backgroundColor = UIColor(red: 76/255, green: 175/255, blue: 80/255, alpha: 1.0)
            labelName.layer.cornerRadius = 20.0
            
            UIGraphicsBeginImageContextWithOptions(labelName.frame.size, true, UIScreen.main.scale)
            labelName.layer.render(in: UIGraphicsGetCurrentContext()!)
            userProfileImage.image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
        }
    }
    
}
