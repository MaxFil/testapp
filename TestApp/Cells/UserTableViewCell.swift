//
//  UserTableViewCell.swift
//  TestApp
//
//  Created by Максим Филимонов on 01.11.2017.
//  Copyright © 2017 Максим Филимонов. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userEmailLabel: UILabel!
    
    var user: User? {
        didSet{
            if let userObject = user {
                self.userNameLabel.text = userObject.name?.capitalized ?? "Anonymous"
                self.userEmailLabel.text = userObject.email?.lowercased() ?? userObject.phone
                self.setupUserImage(with: nil, or: self.userNameLabel.text?.capitalized)
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.userProfileImage.layer.masksToBounds = true
        self.userProfileImage.layer.cornerRadius = self.userProfileImage.frame.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setupUserImage(with image: UIImage?, or name: String?) {
        if let profileImage = image {
            self.userProfileImage.contentMode = .scaleAspectFit
            self.userProfileImage.image = profileImage
        }
        
        if let username = name {
            let labelName = UILabel()
            labelName.frame.size = CGSize(width: 40.0, height: 40.0)
            labelName.textColor = UIColor.white
            labelName.text = String(username.characters.first!)
            labelName.textAlignment = NSTextAlignment.center
            labelName.backgroundColor = UIColor(red: 233/255, green: 30/255, blue: 99/255, alpha: 1.0)
            labelName.layer.cornerRadius = 20.0
            
            UIGraphicsBeginImageContextWithOptions(labelName.frame.size, true, UIScreen.main.scale)
            labelName.layer.render(in: UIGraphicsGetCurrentContext()!)
            userProfileImage.image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
        }
    }
}
