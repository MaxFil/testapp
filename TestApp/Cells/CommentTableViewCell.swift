//
//  CommentTableViewCell.swift
//  TestApp
//
//  Created by Максим Филимонов on 31.10.2017.
//  Copyright © 2017 Максим Филимонов. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    
    @IBOutlet weak var commentPostTitleLabel: UILabel!
    @IBOutlet weak var titleCommentLabel: UILabel!
    @IBOutlet weak var messageCommentLabel: UILabel!
    
    @IBOutlet weak var commentIdtextField: UITextField!
    
    @IBOutlet weak var getButton: UIButton!
    
    var dataSource: UIViewController?
    
    var loadingView: LoadingView?
    
    @IBAction func getCommentButton(_ sender: UIButton) {
        if let dataSource = dataSource as? FeedTableTableViewController {
            guard commentIdtextField.text?.isEmpty == false else {
                dataSource.dismissKeyboard()
                return
            }
            
            if let requestCommentId = Int(commentIdtextField.text!) {
                if requestCommentId <= 500 && requestCommentId >= 1 {
                    dataSource.dismissKeyboard()
                    loadingView(isShow: true)
                    dataSource.fetchData(of: .Comment, with: requestCommentId)
                    self.commentIdtextField.text = ""
                }
                else {
                    shake(textField: commentIdtextField)
                }
            }
            
            
            
        }
    }
    
    
    var comment: Comment? {
        didSet{
            loadingView(isShow: false)
            if let commentObject = comment {
                var email = commentObject.email?.lowercased() ?? "No name"
                email.append(" commented on")
                
                let eventText = "commented on"
                
                let range = (email as NSString).range(of: eventText)
                
                let attribute = NSMutableAttributedString.init(string: email)
                attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.gray , range: range)
                
                self.userNameLabel.attributedText = attribute
                self.commentPostTitleLabel.text = commentObject.commentPost?.title ?? "No title"
                self.titleCommentLabel.text = commentObject.name ?? "No comment title"
                self.messageCommentLabel.text = commentObject.body ?? "No comment text"
                self.setupUserImage(with: nil, or: self.userNameLabel.text?.capitalized)
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.loadingView = LoadingView.instanceFromNib()
        
        self.userProfileImage.layer.masksToBounds = true
        self.userProfileImage.layer.cornerRadius = self.userProfileImage.frame.height/2
        
        self.getButton.layer.masksToBounds = true
        self.getButton.layer.cornerRadius = 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func loadingView(isShow: Bool) {
        if isShow{
            if let loadingView = self.loadingView {
                loadingView.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.width, height: self.frame.height)
                self.contentView.addSubview(loadingView)
                self.layoutIfNeeded()
                loadingView.activityIndicator.startAnimating()
                loadingView.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.95)
                
            }
        }
        else {
            if let loadingView = self.loadingView {
                loadingView.removeFromSuperview()
            }
        }
    }
    
    private func setupUserImage(with image: UIImage?, or name: String?) {
        if let profileImage = image {
            self.userProfileImage.contentMode = .scaleAspectFit
            self.userProfileImage.image = profileImage
        }
        
        if let username = name {
            let labelName = UILabel()
            labelName.frame.size = CGSize(width: 40.0, height: 40.0)
            labelName.textColor = UIColor.white
            labelName.text = String(username.characters.first!)
            labelName.textAlignment = NSTextAlignment.center
            labelName.backgroundColor = UIColor(red: 156/255, green: 39/255, blue: 176/255, alpha: 1.0)
            labelName.layer.cornerRadius = 20.0
            
            UIGraphicsBeginImageContextWithOptions(labelName.frame.size, true, UIScreen.main.scale)
            labelName.layer.render(in: UIGraphicsGetCurrentContext()!)
            userProfileImage.image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
        }
    }
    
    private func shake(textField: UITextField) {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.08
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: textField.center.x - 2, y: textField.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: textField.center.x + 2, y: textField.center.y))
        textField.layer.add(animation, forKey: "position")
    }
}
